import { Component } from '@angular/core';
import { Validators } from '@angular/forms';
import { BehaviorSubject, Observable } from 'rxjs';
import { delay, map, tap } from 'rxjs/operators';
import { Item } from './form/components/select-field/select-field.component';
import { ValidatorsApp } from './validatosapp';

export interface DataSelect{
  key: string;
  value:string,
  desc: string;
}


export function createData():DataSelect[]{
  return [
    {key: '1', value: '11', desc:'desc data 11' },
    {key: '1', value: '12', desc:'desc data 12' },
    {key: '1', value: '13', desc:'desc data 13' },
    {key: '2', value: '21', desc:'desc data 21' },
    {key: '2', value: '22', desc:'desc data 22' },
    {key: '2', value: '23', desc:'desc data 23' },
    {key: '3', value: '31', desc:'desc data 31' },
    {key: '3', value: '32', desc:'desc data 32' },
    {key: '3', value: '33', desc:'desc data 33' },
    
  ];
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app-dialog';
  valueForm:any;
  valueSelect: any;
  valueSelectComplex: any;
  subject= new BehaviorSubject<DataSelect[]>(createData());


  dataSelect$: Observable<Item[]>;


    constructor(){
      this.dataSelect$= this.subject.pipe(
        tap(values=> console.log('values',values)),
        map(data=> data.map( d => d as Item)),
        delay(2000)
      ) as Observable<Item[]>;
         
    }

  updateData(key:string){
    if(!key) return;
    this.subject.next(createData().filter(d=> d.key===key))
  }
  
  get validations(){
    return [Validators.required]
  }

  validationsGroup(f1:string,f2:string){
    return [ValidatorsApp.compareFields(f1,f2)]
  }
  validationsInGroup(f1:string,f2:string,f3:string){
    return [ValidatorsApp.compareFieldsGroup(f1,f2),ValidatorsApp.compareFieldsGroupb(f1,f3)]
  }

  validationsInOtherGroup(f1:string,f2:string,f1g:string|null,f2g:string|null){
    return [ValidatorsApp.compareFieldsOtherGroup(f1,f2,f1g,f2g)]
  }
}
