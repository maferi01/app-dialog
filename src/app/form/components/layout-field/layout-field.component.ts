import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-layout-field',
  templateUrl: './layout-field.component.html',
  styleUrls: ['./layout-field.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LayoutFieldComponent implements OnInit {
  @Input()
  parentFormGroup!: FormGroup;
  @Input()
  control!: AbstractControl;
  @Input()
  labelTop!:boolean;

  constructor() { }

  ngOnInit(): void {
  }
  get errors(){
    return Object.keys(this.control.errors as object);
  }
}
